#!/bin/bash

git init
git remote add origin https://gitlab.dlr.de/{{cookiecutter.git_username}}/{{cookiecutter.project_slug}}.git
git add .
git commit -m "Initial commit"
git push -u origin main
